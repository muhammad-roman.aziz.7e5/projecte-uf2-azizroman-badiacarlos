package ui

import model.User
import java.util.*

class UserUI (val sc:Scanner){

    fun showUserMenu(){
        println("Users:\n" +
                "1: Add user\n" +
                "2: Show my user\n" +
                "3: View users\n" +
                "4: Update user\n" +
                "5: Delete user\n" +
                "6: Change user\n" +
                "7: Show statistics\n" +
                "0: Return to main menu")
        chooseMenuUser()
    }

    private fun chooseMenuUser(){
        val ui = UI()
        val optionSubmenu = sc.nextInt()
        when (optionSubmenu){
            0 -> ui.showMenu()
            1 -> adduser()

            3-> viewusers()
            4 -> updateuser()
            5-> deleteuser()

            7-> stats()
        }
    }
    private fun adduser(){
        println("\nEnter username:")
    sc.nextLine()
        val userName = sc.nextLine()
        val user = User(userName)
        filmitb.userlist.add(user)
        showUserMenu()

    }

    private fun viewusers(){
        println("\nShowing users:")
        println(filmitb.userlist)
        showUserMenu()
    }

    private fun updateuser(){
        println("\nChoose user to Update:")
        val numberchange = sc.nextInt()
        sc.nextLine()
        println("\nChange user to:")
        filmitb.userlist[numberchange]= User(sc.nextLine())
        println("\nUser Updated")
        showUserMenu()

    }

    private fun deleteuser(){
        println("\nChoose user to delete:")
        filmitb.userlist.removeAt(sc.nextInt())
        println("\nUser deleted")
        showUserMenu()
    }

fun stats(){

    println("Added films: ${filmitb.filmlist.size}")
    println("Watched films: ") //falta añadir
    if (filmitb.filmlist.size >= 3){
        println("First 3 added films:")
        println(filmitb.filmlist[0])
        println(filmitb.filmlist[1])
        println(filmitb.filmlist[2])

        println("Last 3 added films:")
        println(filmitb.filmlist[filmitb.filmlist.lastIndex])
        println(filmitb.filmlist[filmitb.filmlist.lastIndex-1])
        println(filmitb.filmlist[filmitb.filmlist.lastIndex-2])
    }
    showUserMenu()

}

}
