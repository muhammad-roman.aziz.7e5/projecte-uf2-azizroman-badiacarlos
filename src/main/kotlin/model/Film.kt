package model

import kotlinx.serialization.Serializable
@Serializable
data class Film (var title:String,
                 var director:String,
                 var mainActor: String,
                 var genere: String,
                 var length: Int,
                 ){

}