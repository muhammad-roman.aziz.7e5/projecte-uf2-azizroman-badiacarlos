package model

class FilmItb (var filmlist :MutableList<Film> = mutableListOf(),
               var userlist :MutableList<User> = mutableListOf())
{
    val filmItbStorage = FilmItbStorage()
    init {

        filmlist = filmItbStorage.loadFilms()
        userlist = filmItbStorage.loadUsers()
    }
}
